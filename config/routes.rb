Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
   concern :paginatable do
    get '(page/:page)', action: :index, on: :collection, as: ''
  end

  resources :users, :products, :orders, :product_details, :product_variants, :order_details, :variants, :variant_values, concerns: :paginatable
  post "/login", to: "authencations#login"
end
