class CreateProductDetails < ActiveRecord::Migration[6.0]
  def change
   create_table :product_details do |t|
      t.integer :product_id
      t.integer :price
      t.timestamps
    end
  end
end
