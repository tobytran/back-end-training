class AddQuantityColumnToOrderDetails < ActiveRecord::Migration[6.0]
  def change
    add_column :order_details, :quantity, :integer
  end
end
