class CreateProductVariants < ActiveRecord::Migration[6.0]
  def change
     create_table :product_variants do |t|
      t.integer :product_detail_id
      t.integer  :variant_value_id
      t.timestamps
    end
  end
end
