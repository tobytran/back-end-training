module Products
    class Show
        prepend SimpleCommand
        def initialize(params)
            @params = params[:id]
        end
        attr_accessor :params
        def call
            product = Product.includes(product_details: {product_variants: :variant_value})
                             .find_by_id(params)
            return errors.add(404, " Not found") if product.nil?
            ProductEntity.represent(product)
        end
        
    end
end