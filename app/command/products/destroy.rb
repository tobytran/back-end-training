module Products
    class Destroy
        prepend SimpleCommand
        def initialize(id)
            @id = id
        end
        attr_accessor  :id
        def call
            # binding.pry
            # validate = ProductValidates::CreateValidator.call(params)
            product = Product.find_by_id(id)
            return errors.add(404, " Not found") if product.nil?
            product.destroy
        end
      
    end
end