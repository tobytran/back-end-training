module Products
    class Create
        prepend SimpleCommand
        def initialize(params)
            @params = params
        end
        attr_accessor :params
        def call
            if product = Product.create(params)
                ProductEntity.represent(product)
            else
                errors.add(422, "Parammeter Error")
            end
        end
      
    end
end