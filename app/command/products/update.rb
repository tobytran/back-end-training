module Products
    class Update
        prepend SimpleCommand
        def initialize(params,id)
            @params = params
            @id =id
        end
        attr_accessor :params, :id
        def call
            # validate = ProductValidates::CreateValidator.call(params)
            product = Product.find_by_id(id)
            return errors.add(404, " Not found") if product.nil?
            if product.update(params)
                product
            else 
                errors.add(422, "Parammeter Error")
            end
        end
      
    end
end