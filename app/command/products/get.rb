module Products
    class Get
        prepend SimpleCommand
        def initialize(params)
            @current_page = params[:page] ||= 1
            @params = params
            @per_page  = 2 
        end
        attr_accessor :current_page, :params, :per_page
        def call
            products = Product.page(current_page).per(per_page)
                              .includes(product_details: {product_variants: :variant_value})
                              .search(params)
            page = {
                limit_value: products.limit_value,
                total_pages: products.total_pages,
                current_page:products.current_page,
                next_page: products.next_page,
                prev_page: products.prev_page         
            }
            return {product: ProductEntity.represent(products), page: page}
        end
    end
end