module Orders
    class Destroy
        prepend SimpleCommand
        def initialize(id)
            @id = id
        end
        attr_accessor  :id
        def call
            order = Order.find_by_id(id)
            return errors.add(404, " Not found") if order.nil?
            order.destroy
        end
      
    end
end