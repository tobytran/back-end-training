module Orders
    class Update
        prepend SimpleCommand
        def initialize(params,id)
            @params = params
            @id =id
        end
        attr_accessor :params, :id
        def call
            #  validate = ProductValidates::CreateValidator.call(params: params)
            order = Order.find_by_id(id)
            return errors.add(404, " Not found") if order.nil?
            if order.update(params)
                OrderEntity.represent(order)      
            else
                errors.add(422, "Parammeter Error")
            end

        end
      
    end
end