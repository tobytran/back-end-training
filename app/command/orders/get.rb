module Orders
    class Get
        prepend SimpleCommand
        def initialize(params)
            @current_page = params[:page] ||= 1
            @params = params
            @per_page  = 2 
        end
        attr_accessor :current_page, :params, :per_page
        def call
            orders = Order.page(current_page).per(per_page).includes(:order_details,:user).search(params).references(:user)
            page = {
                    limit_value: orders.limit_value,
                    total_pages: orders.total_pages,
                    current_page:orders.current_page,
                    next_page: orders.next_page,
                    prev_page: orders.prev_page         
                }
            return { order: OrderEntity.represent(orders) , page: page}
        end
    end
end