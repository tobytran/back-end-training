module Orders
    class Show
        prepend SimpleCommand
        def initialize(params)
            @id = params[:id]
        end
        attr_accessor :id
        def call
            order = Order.includes(:order_details).find_by_id(id)
            return errors.add(404, " Not found") if order.nil?
            OrderEntity.represent(order)      
        end
        
    end
end