module Orders
    class Create
        prepend SimpleCommand
        def initialize(params)
            @params = params
        end
        attr_accessor :params
        def call
            if order = Order.create(params)
                OrderEntity.represent(order)      
            else
                errors.add(422, "Parammeter Error")
            end
        end
      
    end
end