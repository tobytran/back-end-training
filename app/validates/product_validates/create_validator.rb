module ProductValidates
    class CreateValidator < ParamsChecker::BaseParamsChecker
        def schema
            {
                name: char_field(min_length: 5, max_length: 255),
                category: char_field(min_length: 3, max_length:255)
            }
        end
        def check_name(name)
            add_error('This name is already exists.') unless Product.where('name = ?', name).blank?
            return name
        end
    end
end