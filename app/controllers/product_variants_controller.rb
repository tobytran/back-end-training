class ProductVariantsController < ApplicationController
    def index
        product_variants = ProductVariant.includes(product_detail: :product).as_json(include: {product_detail:{include: :product}})
        render json: {product_variants: product_variants}, status: 200
    end
    def show
        product_variant = ProductVariant.includes(product_detail: :product).where("id = ?", params[:id]).as_json(include: {product_detail:{include: :product}})
        return  render json: {'messages': 'Cannot find product_details'} if  product_variant.blank?
        render json:  product_variant 
    end
    # def create 
    #     begin
    #         params[:variant_value_ids].each do |id|
    #             ProductVariant.new({"product_detail_id" => params[:product_detail_id],"variant_value_id" =>id}).save
    #         end
    #         respond_to do |format|
    #             format.json {render json: {'messages': 'Create product variant Successful'}}
    #         end
    #     rescue  Exception => e
    #         respond_to do |format|
    #             format.json {render json: {'messages': 'Create product variant fail'}}
    #         end
    #     end
    # end
    # def update
    #     @product_variant = ProductVariant.find_by_id(params[:id])
    #    if @product_variant.update(product_variant_params)
    #         respond_to do |format|
    #             format.json {render json: {'messages': 'Update product variant Successful','product': @product_variant}}
    #         end
    #    else 
    #         respond_to do |format|
    #             format.json {render json: {'messages': 'Update product variant fail'}}
    #         end
    #    end
    # end
    def destroy
        product_variant = ProductVariant.find_by_id(params[:id])
        if product_variant
            return render json: {'messages': 'Destroy product_variant fail'}  unless product_variant.destroy
            render json: {'messages': 'Destroy product_variant Successful'}  

        else
            render json: {'messages': 'Cannot find product_variant'} 
        end
     end
    # private
    # def product_variant_params
    #   params.permit(:product_detail_id, :variant_value_id )
    # end
end
