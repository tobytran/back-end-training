
class ProductsController < ApplicationController
    before_action :authentication

    def index
        command = Products::Get.call(params)
        return  render json: {errors: command.errors} unless command.success?
        render json: command.result
    end

    def show
        command = Products::Show.call(params)
        return  render json: {errors: command.errors},status: 404 unless command.success?
        render json: command.result,status: 200
    end

    def create 
        command = Products::Create.call(product_params)
        return  render json: {errors: command.errors},status: 422 unless command.success?
        render json: command.result,status: 200
    end

    def update
        command = Products::Update.call(product_params,params[:id])
        return  render json: {errors: command.errors} unless command.success?
        render json: {'product':command.result,'message': 'update product success'}, status: 200
    end

    def destroy
        command = Products::Destroy.call(params[:id])
        return  render json: {errors: command.errors},status: 404 unless command.success?
        render json: {'message': 'Delete product success'},status: 200
    end

    private
    def product_params
        {
          name: params[:name],
          category: params[:category],
          product_details_attributes: params[:product_details]
            
        } 
    end
end
