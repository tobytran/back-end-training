class VariantValuesController < ApplicationController
    def index
        variant_values = VariantValue.all
        respond_to do |format|
            format.json {render json: @variant_values}
        end
    end
    def create
        variant_value = VariantValue.new(variant_value_params)
        if @variant_value.save
            respond_to do |format|
                format.json {render json: {'messages': 'Create Variant Value Successful', 'variant': variant_value}}
            end
        else 
            respond_to do |format|
            format.json {render json: {'messages': 'Create User fail','variant': variant_value}}
            end
        end
    end
    private
    def variant_value_params
      params.permit(:variant_id,:value)
    end
end
