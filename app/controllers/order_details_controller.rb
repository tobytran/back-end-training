class OrderDetailsController < ApplicationController
    def index
        order_details = OrderDetail.includes(:order).as_json(include: :order)
        render json: {order_details: order_details}
    end
    def show
        order_detail = OrderDetail.includes(:order).where("id = ?",params[:id]).as_json(include: :order)
        return render json: {'messages': 'Cannot find order_detail'} if  order_detail.blank? 
        render json: order_detail  
    end
    # def create
    #     @order = Order.find_by_id(params[:order_id])
    #     @order_detail = @order.order_details.create(order_detail_params)
    #     if @order_detail
    #         respond_to do |format|
    #             format.json {render json: {'messages': 'Create Order detail Successful','Order Detail': @order_detail}}
    #         end
    #    else 
    #         respond_to do |format|
    #             format.json {render json: {'messages': 'UCreate Order detail  fail'}}
    #         end
    #    end
    # end
    # def update
    #     @order_detail = OrderDetail.find_by_id(params[:id])
    #     if @order_detail.update(order_detail_params)
    #         respond_to do |format|
    #             format.json {render json: {'messages': 'Update Order detail Successful','Order Detail': @order_detail}}
    #         end
    #    else 
    #         respond_to do |format|
    #             format.json {render json: {'messages': 'Update Order detail  fail'}}
    #         end
    #    end
    # end
    def destroy
        order_detail = OrderDetail.find_by_id(params[:id])
        if order_detail
             return render json: {'messages': 'Destroy order_detail fail'}  unless order_detail.destroy
             render json: {'messages': 'Destroy order_detail Successful'}  
        else
             render json: {'messages': 'Cannot find  order_detail destroy'} 
        end
    end
    # private
    # def order_detail_params
    #   params.permit(:product_detail_id, :quantity,)
    # end
end
