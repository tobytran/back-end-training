class UsersController < ApplicationController
    before_action :authentication

    def index
      	 users = User.all
         render json: {user: users}, status: 200
    end

    def show
        user = User.find_by_id(params[:id])
        return  render json: {'messages': 'Cannot find user'} unless  user.present?
        render json:  user
    end

    def create 
         user = User.new(user_params)
        if  user.save
           render json: user
        else 
           render json: {'messages': 'Cannot create user'}
        end
    end

    def update
         user = User.find_by_id(params[:id])
       if user.update(user_params)
          render json: {'messages': 'Update user successfull'}
       else 
           render json: {'messages': 'Cannot update user'}
       end
    end

    def destroy
        user = User.find_by_id(params[:id])
       if user
        return render json: {'messages': 'Destroy user fail'}  unless user.destroy
        render json: {'messages': 'Destroy user Successful'}  
       else
        render json: {'messages': 'Cannot find user destroy'} 
       end
    end

    private
    def user_params
      params.permit(:full_name, :user_name, :password, :role)
    end
end
