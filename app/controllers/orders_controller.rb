
class OrdersController < ApplicationController
    before_action :authentication
    def index
        command = Orders::Get.call(params)
        return  render json: {errors: command.errors} unless command.success?
        render json: command.result, status: 200
    end

    def show
        command = Orders::Show.call(params)
        return  render json: command.errors, status: 404 unless command.success?
        render json: command.result, status: 200
    end

    def create
        command = Orders::Create.call(order_params)
        return  render json: {errors: command.errors},status: 422 unless command.success?
        render json: command.result, status: 200
    end

    def update
        command = Orders::Update.call(order_params,params[:id])
        return  render json: {errors: command.errors} unless command.success?
        render json: {'order':command.result}, status: :ok
    end

  
    def destroy
        command = Orders::Destroy.call(params[:id])
        return  render json: {errors: command.errors}, status:404 unless command.success?
        render json: {'message': 'Delete scuccess'}, status: 201
    end

    private
    def order_params
        {
            user_id: params[:user_id],
            email: params[:email],
            phoneNumber: params[:phoneNumber],
            address: params[:address],
            total: params[:total],
            order_details_attributes: params[:order_details]
        }
        
    end
end


