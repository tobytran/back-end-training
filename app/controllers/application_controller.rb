class ApplicationController < ActionController::Base
   
    protect_from_forgery with: :null_session
  
    def endcode_token(payload)
        JWT.encode(payload, 'secret')
    end
    def auth_header
        # { Authorization: 'Bearer <token>' }
        request.headers['Authorization']
    end

    def decoded_token
        if auth_header
            token = auth_header.split(' ')[1]
            begin
                JWT.decode(token, 'secret', true, algorithm: 'HS256')
            rescue JWT::DecodeError
                nil
            end
           
        end
    end

    def current_user
        if decoded_token
            user_id = decoded_token[0]['user_id']
            user = User.find_by_id(user_id)
        end
        
    end
    def authentication
        @current_user ||= current_user
       return render json: { message: 'Please log in' }, status: :unauthorized if  @current_user.nil? 
       render json: {message: 'Forbidden'}, status: :forbidden unless  @current_user.admin? 
    end
end
