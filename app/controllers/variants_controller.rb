class VariantsController < ApplicationController
    before_action :authentication
    def index
        variants = Variant.all
        respond_to do |format|
            format.json {render json: variants}
        end
    end
    def create 
        variant = Variant.new(variant_params)
        if @variant.save
            respond_to do |format|
                format.json {render json: {'messages': 'Create Variant Successful', 'variant': variant}}
            end
        else 
            respond_to do |format|
            format.json {render json: {'messages': 'Create User fail','variant': vriant}}
            end
        end
    end
    private
    def variant_params  
      params.permit(:name)
    end
end
