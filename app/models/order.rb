class Order < ApplicationRecord
    belongs_to :user
    has_many :order_details, :dependent => :destroy
    has_many :product_details, through: :order_details
    accepts_nested_attributes_for :order_details , allow_destroy: true
    scope :search_name, -> (name) {where("users.full_name = ?",name) if name.present?}
    scope :search_email, -> (email) {where("email =?",email) if email.present?}
    scope :search_address, -> (address) {where("address = ?",address) if address.present?}
    scope :search_total, -> (total){where("total= ?",total) if total.present?}
    scope :search_date, ->(date_start, date_end){where("orders.created_at  > ? AND orders.created_at < ?",Date.parse(date_start).beginning_of_day,Date.parse(date_end).end_of_day) if date_start.present? && date_end.present?}
    scope :search,lambda { |params|
        search_name(params[:name])
        .search_email(params[:email])
        .search_address(params[:address])
        .search_total(params[:total])
        .search_date(params[:date_start],params[:date_end])
    }
end

