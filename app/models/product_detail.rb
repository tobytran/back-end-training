class ProductDetail < ApplicationRecord
    belongs_to :product
    has_many :order_details
    has_many :orders, through: :order_details
    has_many :product_variants
    has_many :variant_values, through: :product_variants
    accepts_nested_attributes_for :product_variants
end

