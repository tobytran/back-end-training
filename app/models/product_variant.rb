class ProductVariant < ApplicationRecord
    belongs_to :product_detail
    belongs_to :variant_value
end

