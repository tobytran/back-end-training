class VariantValue < ApplicationRecord
    belongs_to :variant
    has_many :product_variants
    has_many :product_details, through: :product_variants
end

