class VariantValueEntity <Grape::Entity
    expose :value
    expose :variant, using: VariantEntity
end