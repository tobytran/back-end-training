class ProductVariantEntity < Grape::Entity
    expose :variant_value, using: VariantValueEntity
end