class OrderEntity <  Grape::Entity
    expose :id
    expose :user, using: UserEntity
    expose :email
    expose :phoneNumber
    expose :address
    expose :total
    expose :order_details, using: OrderDetailEntity
    expose :created_at
end