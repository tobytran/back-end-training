class ProductEntity <  Grape::Entity
    expose :id
    expose :name
    expose :category
    expose :product_details, using: ProductDetailEntity

end