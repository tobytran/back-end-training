class OrderDetailEntity <  Grape::Entity
    expose :product_detail,using: ProductDetailEntity
    expose :quantity
end