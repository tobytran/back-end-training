class ProductDetailEntity <  Grape::Entity
    expose :id
    expose :price
    expose :product_variants,  using: ProductVariantEntity
end